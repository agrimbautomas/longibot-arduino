// Motor A
int ENA = 10;
int IN1 = 9;
int IN2 = 8;

// Motor B
int ENB = 5;
int IN3 = 7;
int IN4 = 6;

const int dcMotorSpeed = 200;
const int turnSpeed = dcMotorSpeed / 2;
char receivedDcWheelCommand;

void setupDcMotors () {
  // Declaramos todos los pines como salidas
  pinMode (ENA, OUTPUT);
  pinMode (ENB, OUTPUT);
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);

  delay(500);
  if (debug)
    logDataAsJson("SERVO", "Ready");
}

void moveDcWheelsByCommand(char cmd) {
  if ( cmd == '2' ) //⬆
    forwardDc();
  if ( cmd == '4' ) //⬅
    turnLeftDc();
  if ( cmd == '5' ) //➡
    turnRightDc();
  if ( cmd == '7' ) //⬇
    backwardDc();
  if ( cmd == '0' ) // []
    stopDc();
}


void forwardDc() {
  if (debug)
    logDataAsJson("DC", "=== MOVING DC FORWARD ===");
  moveForwardLeftDc();
  moveForwardRightDc();
}

void backwardDc () {
  if (debug)
    logDataAsJson("DC", "=== MOVING DC BACKWARD ===");
  moveBackwardLeftDc();
  moveBackwardRightDc();
}

void turnRightDc() {
  if (debug)
    logDataAsJson("DC", "=== MOVING DC RIGHT ===");
  moveForwardLeftDc();
  moveBackwardRightDc();
}

void turnLeftDc() {
  if (debug)
    logDataAsJson("DC", "=== MOVING DC LEFT ===");
  moveBackwardLeftDc();
  moveForwardRightDc();
}

void stopDc() {
  if (debug)
    logDataAsJson("DC", "=== STOP DC MOTORS ===");
  stopLeftDc();
  stopRightDc();
}

// Turn by Degrees
void turnLeftDc90() {
  if (debug)
    logDataAsJson("DC", "=== MOVING DC LEFT 45 ===");
  moveBackwardLeftDc();
  moveForwardRightDc();
  delay(2000);
}

// Left Dc motor
void moveForwardLeftDc() {
  digitalWrite (IN1, HIGH);
  digitalWrite (IN2, LOW);
  updateLeftDc(dcMotorSpeed);
}

void moveBackwardLeftDc() {
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, HIGH);
  updateLeftDc(dcMotorSpeed);
}

void stopLeftDc() {
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, LOW);
  updateLeftDc(0);
}

void updateLeftDc(int updatedSpeed) {
  analogWrite (ENA, updatedSpeed);
}


// Right Dc motor
void moveForwardRightDc() {
  //Direccion motor B
  digitalWrite (IN3, HIGH);
  digitalWrite (IN4, LOW);
  analogWrite (ENB, dcMotorSpeed);
}

void moveBackwardRightDc() {
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, HIGH);
  updateRightDc(dcMotorSpeed);
}
void stopRightDc() {
  //Direccion motor B
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, LOW);
  updateRightDc(0);
}

void updateRightDc(int updatedSpeed) {
  analogWrite (ENB, updatedSpeed);
}
