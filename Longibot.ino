
char receivedCommand = 0;
bool debug = true;
char receivedWheelCommand;

void setup() {

  if (debug) {
    Serial.begin(9600);
    logDataAsJson("SERIAL", "Ready");
  }

  //setupWheels();
  setupServo();
  //setupBluetooth();
  setupUltrasonic();
  setupDcMotors();
  setupTemperature();
}

void loop() {
  //moveDcWheelsBySerialCommand();
  //automaticRunByDistance();
  //runCommandsWithBT();
  //moveWheelsBySerialCommand();
  controlBotBySerial();
}

void controlBotBySerial() {
  if (Serial.available() > 0)
    controlBotByCmd(Serial.read());
}

void controlBotByCmd(char cmd) {
  if (debug)
    logDataAsJson("control", String(cmd));

  moveServoByCommand(cmd);
  makeAutomaticByCommand(cmd);
  moveDcWheelsByCommand(cmd);
  readTemperatureHumidityByCommand(cmd);
}

void logDataAsJson(String key, String data) {
  if (data != "" && data != "\n")
    Serial.println("{\"" + key + "\": \"" + data + "\"}");
}
