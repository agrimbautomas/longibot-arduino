#include <Servo.h>

Servo steerServo;

const byte servoPin = 3;
int servoStep = 2;
int pos = 0;

void setupServo() {
  steerServo.attach(servoPin);
  delay(500);
  if (debug)
    logDataAsJson("SERVO", "Ready");
  moveServo(60);
}

void moveServoByCommand(char cmd) {
  if ( cmd == 'U' ) // Up
    moveServoUp();
  if ( cmd == 'D' ) // Down
    moveServoDown();
}

void moveServoUp() {
  moveServo(servoStep);
  if (debug)
    logDataAsJson("SERVO", "Moving Up");
}

void moveServoDown() {
  moveServo(-servoStep);
  if (debug)
    logDataAsJson("SERVO", "Moving Down");
}

void moveServo(int degreesPosition) {
  if ( (pos + degreesPosition) <= 180 && (pos + degreesPosition) >= 0 )
    pos = pos + degreesPosition;
    
  steerServo.write(pos);
  if (debug)
    logDataAsJson("SERVO", "Position: " + String(pos));
}
