/*
  Longibot
*/

#include <Stepper.h>

const int stepsPerWheelSpin = 32 * 64; // change this to fit the number of steps per revolution
const int halfStepsPerWheelSpin = stepsPerWheelSpin / 2;
const int motorSpeed = 15;

Stepper rightWheel(stepsPerWheelSpin, 8, 10, 9, 11); //Number 11 is shared
Stepper leftWheel(stepsPerWheelSpin, 3, 5, 4, 11);

void setupWheels() {
  leftWheel.setSpeed(motorSpeed);
  rightWheel.setSpeed(motorSpeed);
  delay(500);

  if (debug)
      logDataAsJson("STEPPERS", "Ready");
}

void moveWheelsByCommand(char cmd) {
  if ( cmd == '2' ) //⬆
    forward();
  if ( cmd == '4' ) //⬅
    turnLeft();
  if ( cmd == '5' ) //➡
    turnRight();
  if ( cmd == '7' ) //⬇
    backward();
}

void moveWheelsBySerialCommand() {
  if (Serial.available() > 0) {
    receivedWheelCommand = Serial.read();
    moveWheelsByCommand(receivedWheelCommand);
  }
}


void forward() {
  if (debug)
    Serial.println("=== MOVING FORWARD ===");
  for (int s = 0; s < stepsPerWheelSpin; s++) {
    forwardRightWheel();
    forwardLeftWheel();
  }
}

void backward() {
  if (debug)
    Serial.println("=== MOVING BACKWARD ===");
  for (int s = 0; s < stepsPerWheelSpin; s++) {
    backwardRightWheel();
    backwardLeftWheel();
  }
}

void turnRight() {
  if (debug)
    Serial.println("=== TURNING RIGHT ===");
  for (int s = 0; s < halfStepsPerWheelSpin; s++) {
    backwardRightWheel();
    forwardLeftWheel();
  }
}

void turnLeft() {
  if (debug)
    Serial.println("=== TURNING LEFT ===");
  for (int s = 0; s < halfStepsPerWheelSpin; s++) {
    forwardRightWheel();
    backwardLeftWheel();
  }
}

// LeftWheel
void forwardLeftWheel() {
  leftWheel.step(-1);
}

void backwardLeftWheel() {
  leftWheel.step(1);
}

// RightWheel
void forwardRightWheel() {
  rightWheel.step(-1);
}

void backwardRightWheel() {
  rightWheel.step(1);
}
