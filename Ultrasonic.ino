
// defines pins numbers
const int trigPin = 12;
const int echoPin = 13;

// defines variables
long duration;
int cmDistance;
const int distanceLimit = 50;

void setupUltrasonic() {
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  delay(500);
  if (debug)
    logDataAsJson("ULTASONIC", "Ready");
}

void makeAutomaticByCommand(char cmd){
  if ( cmd == 'A' ) //⬆
    automaticRunByDistance();
}

void automaticRunByDistance() {
  if (debug)
    logDataAsJson("MODE", "ULTASONIC");

  runByDistance();
}

void runByDistance() {
  while (!stopIsNeeded()) {
    // Clears the trigPin
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);

    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);

    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration = pulseIn(echoPin, HIGH);
    turnIfClose(distanceInCm(duration));

    if (debug)
      logDataAsJson("ULTASONIC - Distance", String(distanceInCm(duration)));
  }

  stopDc();
}


bool stopIsNeeded() {
  bool isNeeded = false;
  if (Serial.available() > 0 ) {
    char cmd = Serial.read();
    isNeeded = (String(cmd) == "0");
  }

  return isNeeded;
}

int distanceInCm(long duration) {
  return duration * 0.034 / 2;
}


void turnIfClose(int cmDistance) {
  if (cmDistance <= distanceLimit)
    turnLeftDc90();
  else
    forwardDc();
}
