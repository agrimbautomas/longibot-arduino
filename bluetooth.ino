#include <SoftwareSerial.h>

SoftwareSerial miBT(6, 7);

void setupBluetooth() {
  miBT.begin(38400);    // comunicacion serie entre Arduino y el modulo a 38400 bps
  if (debug)
    logDataAsJson("BLUETOOTH", "Ready");
}

void runCommandsWithBT() {
  if (miBT.available()) {
    receivedCommand = miBT.read();

    if (debug)
      logDataAsJson("BT", String(receivedCommand));

    //moveServoByCommand(receivedCommand);
    //moveWheelsByCommand(receivedCommand);
    receivedCommand = 0;
  }
}
