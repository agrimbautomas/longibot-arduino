#include "DHT.h"
#define DHTPIN 4
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

void setupTemperature() {
  dht.begin();
  delay(500);
  if (debug)
    logDataAsJson("SERVO", "Ready");
}

void readTemperatureHumidityByCommand(char cmd) {
  if ( cmd == 'T' )
    readTemperatureHumidity();
}


void readTemperatureHumidity() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);

  if (isnan(h) || isnan(t) || isnan(f)) {
    logDataAsJson("error", "Temperature Error: Failed to read from DHT sensor!");
    return;
  }

  float hif = dht.computeHeatIndex(f, h);
  float hic = dht.computeHeatIndex(t, h, false);

  logDataAsJson("temp/hum", String(h) + "/" + String(t) );

}
